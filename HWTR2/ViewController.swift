//
//  ViewController.swift
//  HWTR2
//
//  Created by Олександр Кахерский on 30.08.2018.
//  Copyright © 2018 Олександр Кахерский. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        buildPyramid(number: 3)
        
    }
    
    func buildPyramid(number: Int) {
        let step = 40
        var x = 144
        var y = 0
        var count = 0
        for _ in 0..<number {
            y += step
            count += 1
            for _ in 0..<count {
                buildCube(x: x, y: y)
                x += step
            }
            x = x - 20 - (40 * count)
        }
    }
    
    func buildLedder(number: Int) {
        let step = 40
        var x = 40
        var y = 0
        var count = 0
        for _ in 0..<number {
            y += step
            count += 1
            for _ in 0..<count {
                buildCube(x: x, y: y)
                x += step
            }
            x = 40
        }
    }
    
    func buildLine(number: Int) {
        let y = 40
        var x = 0
        for _ in 0..<number {
            x += 40
            buildCube(x: x, y: y)
        }
    }
    
    func buildCube(x: Int, y: Int) {
        let size = 32
        let rect = CGRect.init(x: x, y: y, width: size, height: size)
        let box = UIView.init(frame: rect)
        box.backgroundColor = UIColor.gray
        view.addSubview(box)
    }
    
}

